import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import axios from 'axios';
import StarwarsYellow from '../assets/img-starwars-yellow';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataFilms: [],
      dataVehicles: [],
      dataStarships: [],
    };
  }

  componentDidMount() {
    let arrayFilms = [];
    let arrayVehicles = [];
    let arrayStarships = [];
    console.log('ini adalah dataprofile : ', this.props.dataProfile);
    let p = this.props.dataProfile;
    //MAPPING DATA FILMS
    if (p['films']) {
      for (key in p['films']) {
        axios
          .get(p['films'][key])
          .then(response => {
            arrayFilms.push(response.data);
            if (arrayFilms.length === p['films'].length) {
              this.setState(
                {
                  dataFilms: arrayFilms,
                },
                () => console.log('INI ADALAH ARRAY Films : ', arrayFilms),
              );
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
    //MAPPING DATA VEHICLES
    if (p['vehicles']) {
      for (key in p['vehicles']) {
        axios
          .get(p['vehicles'][key])
          .then(response => {
            arrayVehicles.push(response.data);
            if (arrayVehicles.length === p['vehicles'].length) {
              this.setState(
                {
                  dataVehicles: arrayVehicles,
                },
                () =>
                  console.log('INI ADALAH ARRAY Vehicles : ', arrayVehicles),
              );
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
      //MAPPING DATA STARSHIPS
      if (p['starships']) {
        for (key in p['starships']) {
          axios
            .get(p['starships'][key])
            .then(response => {
              arrayStarships.push(response.data);
              if (arrayStarships.length === p['starships'].length) {
                this.setState(
                  {
                    dataStarships: arrayStarships,
                  },
                  () =>
                    console.log(
                      'INI ADALAH ARRAY Starships : ',
                      arrayStarships,
                    ),
                );
              }
            })
            .catch(function (error) {
              console.log(error);
            });
        }
      }
    }
  }

  render() {
    let p = this.props.dataProfile;
    return (
      <ScrollView nestedScrollEnabled={true}>
        <View style={styles.container}>
          <Text
            style={{
              fontFamily: 'Inter-SemiBold',
              color: '#25282B',
              fontSize: 21,
              paddingRight: 24,
            }}>
            Profile
          </Text>
          <View
            style={{
              backgroundColor: '#25282B',
              height: '10%',
              borderRadius: 4,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
              marginRight: 24,
            }}>
            <Image
              source={require('../assets/img-starwars-white.png')}
              style={{
                height: '40%',
                width: '40%',
                resizeMode: 'contain',
                flex: 1,
              }}
            />
          </View>
          <View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 20,
                paddingRight: 24,
              }}>
              <Text style={{fontFamily: 'Inter-Regular', color: '#9E9E9E'}}>
                Name{' '}
              </Text>
              <Text style={{color: '#25282B'}}>{p['name']}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 10,
                paddingRight: 24,
              }}>
              <Text style={{color: '#9E9E9E'}}>Height</Text>
              <Text style={{color: '#25282B'}}>{p['height']} cm</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 10,
                paddingRight: 24,
              }}>
              <Text style={{color: '#9E9E9E'}}>Birth Year </Text>
              <Text style={{color: '#25282B'}}>{p['birth_year']}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 10,
                paddingRight: 24,
              }}>
              <Text style={{color: '#9E9E9E'}}>Gender</Text>
              <Text style={{color: '#25282B'}}>
                {p['gender'].toUpperCase()}
              </Text>
            </View>
          </View>
          <Text
            style={{
              fontFamily: 'Inter-SemiBold',
              color: '#25282B',
              fontSize: 21,
              marginTop: 24,
              marginBottom: 10,
            }}>
            Films
          </Text>
          <View style={{borderColor: 'black'}}>
            <ScrollView horizontal>
              {this.state.dataFilms.map((item, index) => (
                <View key={index} style={styles.cardParent}>
                  <View
                    style={{
                      backgroundColor: '#25282B',
                      height: 140,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderTopLeftRadius: 8,
                      borderTopRightRadius: 8,
                    }}>
                    <StarwarsYellow />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: 'white',
                      height: 20,
                      justifyContent: 'center',
                      paddingLeft: 10,
                      borderBottomLeftRadius: 8,
                      borderBottomRightRadius: 8,
                      // shadowColor: '#000',
                      // shadowOffset: {width: 1, height: 1},
                      // shadowOpacity: 0.4,
                      // shadowRadius: 3,
                      // elevation: 5,
                    }}>
                    <Text
                      style={{
                        color: '#25282B',
                        //   fontWeight: 'bold',
                        fontSize: 14,
                        fontFamily: 'Inter-SemiBold',
                      }}>
                      Star Wars: {item['title']}
                    </Text>
                  </View>
                </View>
              ))}
            </ScrollView>
          </View>
          <Text
            style={{
              fontFamily: 'Inter-SemiBold',
              color: '#25282B',
              fontSize: 21,
              marginTop: 24,
              marginBottom: 10,
            }}>
            Vehicles
          </Text>
          <View style={{borderColor: 'black', width: '100%', paddingRight: 24}}>
            {this.state.dataVehicles.map((item, index) => (
              <View
                style={{
                  borderRadius: 8,
                  height: 80,
                  backgroundColor: 'white',
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                  shadowColor: '#000',
                  shadowOffset: {width: 1, height: 1},
                  shadowOpacity: 0.4,
                  shadowRadius: 3,
                  elevation: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 10,
                }}
                key={index}>
                <View
                  style={{
                    backgroundColor: '#25282B',
                    height: 60,
                    width: 60,
                    borderRadius: 4,
                    marginLeft: 10,
                    marginRight: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('../assets/img-starwars-white.png')}
                    style={{
                      height: '70%',
                      width: '70%',
                      resizeMode: 'contain',
                      flex: 1,
                    }}
                  />
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        color: '#25282B',
                        fontSize: 14,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['name']}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: '#9E9E9E',
                        fontSize: 12,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['model']}
                    </Text>
                    <Text
                      style={{
                        color: '#9E9E9E',
                        fontSize: 12,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['manufacturer']}
                    </Text>
                  </View>
                </View>
              </View>
            ))}
          </View>
          <Text
            style={{
              fontFamily: 'Inter-SemiBold',
              color: '#25282B',
              fontSize: 21,
              marginBottom: 10,
              marginTop: 14,
            }}>
            Starships
          </Text>
          <View
            style={{
              borderColor: 'black',
              width: '100%',
              height: 300,
              paddingRight: 24,
            }}>
            {this.state.dataStarships.map((item, index) => (
              <View
                style={{
                  borderRadius: 8,
                  height: 80,
                  backgroundColor: 'white',
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                  shadowColor: '#000',
                  shadowOffset: {width: 1, height: 1},
                  shadowOpacity: 0.4,
                  shadowRadius: 3,
                  elevation: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 10,
                }}
                key={index}>
                <View
                  style={{
                    backgroundColor: '#25282B',
                    height: 60,
                    width: 60,
                    borderRadius: 4,
                    marginLeft: 10,
                    marginRight: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('../assets/img-starwars-white.png')}
                    style={{
                      height: '70%',
                      width: '70%',
                      resizeMode: 'contain',
                      flex: 1,
                    }}
                  />
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        color: '#25282B',
                        fontSize: 14,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['name']}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: '#9E9E9E',
                        fontSize: 12,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['model']}
                    </Text>
                    <Text
                      style={{
                        color: '#9E9E9E',
                        fontSize: 12,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['manufacturer']}
                    </Text>
                  </View>
                </View>
              </View>
            ))}
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(getData, null)(Profile);

function getData(state) {
  return {
    dataProfile: state.reducer.dataProfile,
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginTop: StatusBar.currentHeight || 0,
    // alignItems: 'center',
    paddingLeft: 24,
    height: '100%',
    paddingBottom: 20,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  highlightfalse: {
    backgroundColor: 'grey',
    height: 100,
    justifyContent: 'center',
  },
  cardParent: {
    height: 200,
    width: 150,
    marginRight: 10,
    // elevation: 10
  },
  highlighttrue: {
    backgroundColor: 'black',
    height: 100,
    justifyContent: 'center',
  },
});
