import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import axios from 'axios';
import StarwarsWhite from '../assets/img-starwars-white';

class Cast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    let arrayTemp = [];
    console.log('ini dalah data cast: ', this.props.dataCast);
    for (key in this.props.dataCast) {
      axios
        .get(this.props.dataCast[key])
        .then(response => {
          arrayTemp.push(response.data);
          if (arrayTemp.length === this.props.dataCast.length) {
            this.setState(
              {
                data: arrayTemp,
              },
              () => console.log('INI ADALAH ARRAY TEMP : ', arrayTemp),
            );
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  goTo = item => {
    console.log('INI ADALAH ITEM : ', item);

    this.props.setDataProfile(item);
    this.props.navigation.navigate('Profile', {name: item['name']});
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text
            style={{
              fontFamily: 'Inter-SemiBold',
              color: '#25282B',
              fontSize: 21,
            }}>
            Cast
          </Text>
          <View style={{width: '100%'}}>
            {this.state.data.map((item, index) => (
              <TouchableOpacity
                style={{
                  marginTop: 20,
                  borderRadius: 8,
                  height: 60,
                  backgroundColor: 'white',
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                  shadowColor: '#000',
                  shadowOffset: {width: 1, height: 1},
                  shadowOpacity: 0.4,
                  shadowRadius: 3,
                  elevation: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => this.goTo(item)}
                key={index}>
                <View
                  style={{
                    backgroundColor: '#25282B',
                    height: 48,
                    width: 48,
                    borderRadius: 4,
                    marginLeft: 10,
                    marginRight: 10,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                    <Image
                  source={require('../assets/img-starwars-white.png')}
                  style={{
                    height: '70%',
                    width: '70%',
                    resizeMode: 'contain',
                    flex: 1,
                  }}
                />
                  </View>
                <View>
                  <View>
                    <Text
                      style={{
                        color: '#25282B',
                        fontSize: 14,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['name']}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: '#9E9E9E',
                        fontSize: 12,
                        fontFamily: 'Inter-Regular',
                      }}>
                      {item['gender'].toUpperCase()}, {item['height']} cm
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(getData, setData)(Cast);

function getData(state) {
  return {
    dataCast: state.reducer.dataCast,
  };
}

var sendDataProfile = function (dataProfile) {
  return {
    type: 'setDataProfile',
    newDataProfile: dataProfile,
  };
};

function setData(dispatch) {
  return {
    setDataProfile: dataProfile => dispatch(sendDataProfile(dataProfile)),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginTop: StatusBar.currentHeight || 0,
    // alignItems: 'center',
    paddingHorizontal: 24,
    height: '100%',
    paddingBottom: 20,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  highlightfalse: {
    backgroundColor: 'grey',
    height: 100,
    justifyContent: 'center',
  },
  highlighttrue: {
    backgroundColor: 'black',
    height: 100,
    justifyContent: 'center',
  },
});
