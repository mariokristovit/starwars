//reducer

const wrapper = {
  title: 'JUDUL SEMAU GW',
  dataCast: [],
};

const reducer = (state = wrapper, action) => {
  switch (action.type) {
    case 'setDataCast':
      return {
        ...state,
        dataCast: action.newDataCast,
      };
    case 'setDataProfile':
      return {
        ...state,
        dataProfile: action.newDataProfile,
      };
  }
  return state;
};

export default reducer;
