## Name
Starwars Project

## Description
This application made for job test purpose at Gokomodo.
The framework used for making this application is React Native.

## Visuals
![Starwars film Screen](https://i.postimg.cc/13B18wRs/Whats-App-Image-2022-06-09-at-11-17-56-2.jpg)
![Starwars cast Screen](https://i.postimg.cc/XvDM8WyP/Whats-App-Image-2022-06-09-at-11-17-56-1.jpg)
![Starwars profile Screen](https://i.postimg.cc/433kVynt/Whats-App-Image-2022-06-09-at-11-17-56.jpg)


## Installation
After running the clone process, run command "npm install" in project directory first, then run the command "react-native run-android" or "npx react-native run-android"

## Usage
After instaling process, just open the Starwars app.

## Support
Email: mariokristovit@gmail.com
Gitlab: https://gitlab.com/mariokristovit

## Authors
@mariokristov

## Project status
Active(Waiting for Gokomodos' Team Review)
