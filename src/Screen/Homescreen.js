import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import axios from 'axios';
import StarswarsBlack from '../assets/img-starwars-black';
import StarwarsYellow from '../assets/img-starwars-yellow';

class Homescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataFilm: [],
      dataFilmTemp: []
    };
  }

  componentDidMount() {
    axios
      .get(`https://swapi.dev/api/films`)
      .then(response => {
        console.log('INI ADALAH RESPON FILMS : ', response.data);
        this.setState({
          dataFilm: response.data.results,
          dataFilmTemp: response.data.results
        });
        console.log('INI ADALAH DATA FILM : ', this.state.dataFilm);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  goTo = item => {
    console.log('INI ADALAH ITEM : ', item);
    this.props.setDataCast(item['characters']);
    this.props.navigation.navigate('Cast', {
      name: `Star Wars: ${item['title']}`,
    });
  };

  searchData(text) {
    let a = this.state.dataFilmTemp
    const newData = a.filter((item, id) => {
        const itemData = item['title'];
        const textData = text;
        return itemData.indexOf(textData) > -1
    });

    this.setState({
        dataFilm: newData,
    })
}

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{height: 100, height: 100}}>
            <StarswarsBlack />
          </View>
          <View
            style={{
              width: '100%',
            //   alignItems: 'center',
            //   justifyContent: 'center',
            //   borderWidth: 0.5,
            //   borderColor: 'white',
              flexDirection: 'row',
              height: 50,
            //   backgroundColor: 'black'
            }}>
            <View
              style={{
                width: '20%',
                // justifyContent: 'center',
                // alignItems: 'center',
                flexDirection: 'row',
                // backgroundColor: 'red'
              }}>
              <View
                style={{
                //   justifyContent: 'center',
                //   alignItems: 'center',
                marginLeft: '50%',
                  height: '100%',
                  width: '100%',
                }}>
                <Image
                  source={require('../assets/search.png')}
                  style={{
                    height: '30%',
                    width: '30%',
                    resizeMode: 'contain',
                    flex: 1,
                  }}
                />
              </View>
            </View>
            <View style={{width: '80%', flexDirection: 'row', height: '100%'}}>
              <TextInput
                style={{
                  color: 'black',
                  width: '100%',
                  fontSize: 14,
                  padding: 0,
                  margin: 0,
                  borderWidth: 0,
                  fontFamily: 'Inter-Regular'
                }}
                onChangeText={(text) => this.searchData(text)}
                maxLength={10}
                placeholder="Search Star Wars Film"
                placeholderTextColor={"#9E9E9E"}
                placehold
                underlineColorAndroid="transparent"
              />
            </View>
          </View>
          <View style={{borderColor: 'black', width: '100%'}}>
            {this.state.dataFilm.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => this.goTo(item)}
                style={styles.cardParent}>
                <View
                  style={{
                    backgroundColor: '#25282B',
                    height: '80%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                  }}>
                  <StarwarsYellow />
                </View>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'white',
                    height: '80%',
                    justifyContent: 'center',
                    paddingLeft: 10,
                    borderBottomLeftRadius: 8,
                    borderBottomRightRadius: 8,
                    shadowColor: '#000',
                    shadowOffset: {width: 1, height: 1},
                    shadowOpacity: 0.4,
                    shadowRadius: 3,
                    elevation: 5,
                  }}>
                  <Text
                    style={{
                      color: '#25282B',
                    //   fontWeight: 'bold',
                      fontSize: 14,
                      fontFamily: 'Inter-Regular'
                    }}>
                    Star Wars: {item['title']}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(getData, setData)(Homescreen);

function getData(state) {
  return {
    title: state.reducer.title,
  };
}

var sendDataCast = function (dataCast) {
  return {
    type: 'setDataCast',
    newDataCast: dataCast,
  };
};

function setData(dispatch) {
  return {
    setDataCast: dataCast => dispatch(sendDataCast(dataCast)),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginTop: StatusBar.currentHeight || 0,
    alignItems: 'center',
    paddingHorizontal: 24,
    height: '100%',
    paddingBottom: 20,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  cardParent: {
    height: 200,
    width: '100%',
    marginTop: 24,
    // elevation: 10
  },
  highlighttrue: {
    backgroundColor: 'black',
    height: 100,
    justifyContent: 'center',
  },
});
