import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Homescreen from '../Screen/Homescreen';
import Cast from '../Screen/Cast';
import Profile from '../Screen/Profile';

const Stack = createNativeStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Homescreen">
        <Stack.Screen
          name="Homescreen"
          component={Homescreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Cast"
          component={Cast}
          options={({route}) => ({
            title: route.params.name,
            headerTitleStyle: {
              fontFamily: 'Inter-SemiBold',
              fontSize: 14,
              color: '#25282B',
            },
          })}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={({route}) => ({
            title: route.params.name,
            headerTitleStyle: {
              fontFamily: 'Inter-Bold',
              fontSize: 14,
              color: '#25282B',
            },
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
